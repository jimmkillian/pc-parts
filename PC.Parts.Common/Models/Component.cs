﻿// <copyright file="Component.cs" company="EveryWare">
//     Copyright(c) EveryWare. All rights reserved.
// </copyright >

namespace PC.Parts.Common.Models
{
    using System;

    /// <summary>
    /// Component class
    /// </summary>
    public class Component : ModelBase<Component>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Component"/> class
        /// </summary>
        public Component()
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the Component ID
        /// </summary>
        public Guid ComponentID { get; set; }

        /// <summary>
        /// Gets or sets the Component Name
        /// </summary>
        public string ComponentName { get; set; }

        /// <summary>
        /// Gets or sets the Component Description
        /// </summary>
        public string ComponentDescription { get; set; }

        /// <summary>
        /// Gets or sets the Component Price
        /// </summary>
        public decimal ComponentPrice { get; set; }

        /// <summary>
        /// Gets or sets the Brand ID
        /// </summary>
        public Guid BrandID { get; set; }

        /// <summary>
        /// Gets or sets the ComponentType ID
        /// </summary>
        public Guid ComponentTypeID { get; set; }

        #endregion
    }
}