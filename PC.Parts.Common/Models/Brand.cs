﻿// <copyright file="Brand.cs" company="EveryWare">
//     Copyright(c) EveryWare. All rights reserved.
// </copyright >

namespace PC.Parts.Common.Models
{
    using System;

    /// <summary>
    /// Brand class
    /// </summary>
    public class Brand : ModelBase<Brand>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Brand"/> class
        /// </summary>
        public Brand()
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the Brand ID
        /// </summary>
        public Guid BrandID { get; set; }

        /// <summary>
        /// Gets or sets the Brand Name
        /// </summary>
        public string BrandName { get; set; }

        #endregion
    }
}