﻿// <copyright file="ComponentType.cs" company="EveryWare">
//     Copyright(c) EveryWare. All rights reserved.
// </copyright >

namespace PC.Parts.Common.Models
{
    using System;

    /// <summary>
    /// ComponentType class
    /// </summary>
    public class ComponentType : ModelBase<ComponentType>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentType"/> class
        /// </summary>
        public ComponentType()
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the Component Type ID
        /// </summary>
        public Guid ComponentTypeID { get; set; }

        /// <summary>
        /// Gets or sets the Component Type Name
        /// </summary>
        public string ComponentTypeName { get; set; }

        #endregion
    }
}