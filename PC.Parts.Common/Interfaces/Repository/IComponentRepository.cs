﻿// <copyright file="IComponentRepository.cs" company="EveryWare">
//     Copyright(c) EveryWare. All rights reserved.
// </copyright >

namespace PC.Parts.Common.Interfaces.Repository
{
    using System;
    using System.Collections.Generic;
    using Common.Models;

    /// <summary>
    /// Component Repository interface
    /// </summary>
    public interface IComponentRepository
    {
        /// <summary>
        /// Gets all Component records
        /// </summary>
        /// <returns>A list of <see cref='Component'/> instances</returns>
        List<Component> Get();

        /// <summary>
        /// Gets a Component record
        /// </summary>
        /// <param name="componentID">The Actor Id</param>
        /// <returns>An instance of the <see cref='Component'/> class</returns>
        Component GetSingle(Guid componentID);

        /// <summary>
        /// Adds a new Component record
        /// </summary>
        /// <param name="component">An instance of the <see cref="Component"/> class</param>
        /// <returns>True if successful</returns>
        bool Insert(Component component);

        /// <summary>
        /// Updates a Component record
        /// </summary>
        /// <param name="component">An instance of the <see cref="Component"/> class</param>
        /// <returns>True if successful</returns>
        bool Update(Component component);

        /// <summary>
        /// Deletes a Component record
        /// </summary>
        /// <param name="componentID">The Component ID</param>
        /// <returns>True if successful</returns>
        bool Delete(Guid componentID);
    }
}