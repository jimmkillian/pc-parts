﻿// <copyright file="IBrandRepository.cs" company="EveryWare">
//     Copyright(c) EveryWare. All rights reserved.
// </copyright >

namespace PC.Parts.Common.Interfaces.Repository
{
    using System;
    using System.Collections.Generic;
    using Common.Models;

    /// <summary>
    /// Brand Repository interface
    /// </summary>
    public interface IBrandRepository
    {
        /// <summary>
        /// Gets all Brand records
        /// </summary>
        /// <returns>A list of <see cref='Brand'/> instances</returns>
        List<Brand> Get();

        /// <summary>
        /// Gets a Brand record
        /// </summary>
        /// <param name="brandID">The Actor Id</param>
        /// <returns>An instance of the <see cref='Brand'/> class</returns>
        Brand GetSingle(Guid brandID);
    }
}