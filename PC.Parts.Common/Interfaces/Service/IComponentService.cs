﻿// <copyright file="IComponentService.cs" company="EveryWare">
//     Copyright(c) EveryWare. All rights reserved.
// </copyright >

namespace PC.Parts.Common.Interfaces.Service
{
    using System;
    using System.Collections.Generic;
    using Models;

    /// <summary>
    /// Component Service interface
    /// </summary>
    public interface IComponentService
    {
        /// <summary>
        /// Gets all Component records
        /// </summary>
        /// <returns>A list of <see cref='Component'/> instances</returns>
        List<Component> Get();

        /// <summary>
        /// Gets a Component record
        /// </summary>
        /// <param name="componentID">The Actor Id</param>
        /// <returns>An instance of the <see cref='Component'/> class</returns>
        Component GetSingle(Guid componentID);

        /// <summary>
        /// Inserts a Component record
        /// </summary>
        /// <param name="component">An instance of the <see cref="Component"/> class</param>
        /// <returns>An instance of the <see cref="ServiceResult{T}"/> class</returns>
        ServiceResult<Component> Insert(Component component);

        /// <summary>
        /// Saves a Component record
        /// </summary>
        /// <param name="component">An instance of the <see cref="Component"/> class</param>
        /// <returns>An instance of the <see cref="ServiceResult{T}"/> class</returns>
        ServiceResult<Component> Update(Component component);

        /// <summary>
        /// Deletes a Component record
        /// </summary>
        /// <param name="componentID">The Actor Id</param>
        /// <returns>An instance of the <see cref="ServiceResult{T}"/> class</returns>
        ServiceResult<Component> Delete(Guid componentID);
    }
}
