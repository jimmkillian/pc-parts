﻿// <copyright file="IComponentTypeService.cs" company="EveryWare">
//     Copyright(c) EveryWare. All rights reserved.
// </copyright >

namespace PC.Parts.Common.Interfaces.Service
{
    using System;
    using System.Collections.Generic;
    using Models;

    /// <summary>
    /// ComponentType Service interface
    /// </summary>
    public interface IComponentTypeService
    {
        /// <summary>
        /// Gets all ComponentType records
        /// </summary>
        /// <returns>A list of <see cref='ComponentType'/> instances</returns>
        List<ComponentType> Get();

        /// <summary>
        /// Gets a ComponentType record
        /// </summary>
        /// <param name="componentTypeID">The Actor Id</param>
        /// <returns>An instance of the <see cref='ComponentType'/> class</returns>
        ComponentType GetSingle(Guid componentTypeID);
    }
}
