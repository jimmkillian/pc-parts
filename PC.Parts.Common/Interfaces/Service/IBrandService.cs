﻿// <copyright file="IBrandService.cs" company="EveryWare">
//     Copyright(c) EveryWare. All rights reserved.
// </copyright >

namespace PC.Parts.Common.Interfaces.Service
{
    using System;
    using System.Collections.Generic;
    using Models;

    /// <summary>
    /// Brand Service interface
    /// </summary>
    public interface IBrandService
    {
        /// <summary>
        /// Gets all Brand records
        /// </summary>
        /// <returns>A list of <see cref='Brand'/> instances</returns>
        List<Brand> Get();

        /// <summary>
        /// Gets a Brand record
        /// </summary>
        /// <param name="brandID">The Brand ID</param>
        /// <returns>An instance of the <see cref='Brand'/> class</returns>
        Brand GetSingle(Guid brandID);
    }
}
