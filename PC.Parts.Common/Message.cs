﻿// <copyright file="Message.cs" company="EveryWare">
//     Copyright(c) EveryWare. All rights reserved.
// </copyright >

namespace PC.Parts.Common
{
    /// <summary>
    /// Message class
    /// </summary>
    public class Message
    {
        /// <summary>
        /// Gets or sets the field name
        /// </summary>
        public string FieldName { get; set; }

        /// <summary>
        /// Gets or sets the message text
        /// </summary>
        public string MessageText { get; set; }
    }
}