﻿// <copyright file="Settings.cs" company="EveryWare">
//     Copyright(c) EveryWare. All rights reserved.
// </copyright >

namespace PC.Parts.Common
{
    using Microsoft.Extensions.Configuration;

    /// <summary>
    /// Settings class
    /// </summary>
    public class Settings
    {
        /// <summary>
        /// Gets the connection string
        /// </summary>
        public static string ConnectionString
        {
            get
            {
                if (string.IsNullOrEmpty(Settings._connectionString))
                {
                    Settings.LoadSettings();
                }

                return Settings._connectionString;
            }
        }

        /// <summary>
        /// Gets or sets the connection string
        /// </summary>
        private static string _connectionString { get; set; }

        /// <summary>
        /// Loads the settings from the appSettings file
        /// </summary>
        private static void LoadSettings()
        {
            var builder = new ConfigurationBuilder().AddJsonFile("appSettings.json");
            var configuration = builder.Build();

            Settings._connectionString = configuration["ConnectionString"];
        }
    }
}
