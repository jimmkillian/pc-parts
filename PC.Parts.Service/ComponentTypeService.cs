﻿// <copyright file="ComponentTypeService.cs" company="EveryWare">
//     Copyright(c) EveryWare. All rights reserved.
// </copyright >

namespace PC.Parts.Service
{
    using System;
    using System.Collections.Generic;
    using Common.Interfaces.Logging;
    using Common.Interfaces.Repository;
    using Common.Interfaces.Service;
    using Common.Models;

    /// <summary>
    /// ComponentType Service class
    /// </summary>
    public class ComponentTypeService : ServiceBase, IComponentTypeService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentTypeService"/> class
        /// </summary>
        /// <param name="componentTypeRepository">An implementation of the <see cref="IComponentTypeRepository"/> interface</param>
        /// <param name="logService">An implementation of the <see cref="ILogService"/> interface</param>
        public ComponentTypeService(IComponentTypeRepository componentTypeRepository, ILogService logService) : base(logService)
        {
            this._componentTypeRepository = componentTypeRepository;
        }

        /// <summary>
        /// Gets an implementation of the <see cref="IComponentTypeRepository"/> interface
        /// </summary>
        private IComponentTypeRepository _componentTypeRepository { get; }

        /// <summary>
        /// Returns a list of all existing ComponentTypes
        /// </summary>
        /// <returns>A List of <see cref="ComponentType"/></returns>
        public List<ComponentType> Get()
        {
            this._logService.Debug("ComponentTypeService.Get called.");

            var componentTypes = new List<ComponentType>();

            componentTypes = this._componentTypeRepository.Get();

            this._logService.Verbose("BrandService.Get returned {0} records.", componentTypes.Count);

            return componentTypes;
        }

        /// <summary>
        /// Returns a ComponentType record
        /// </summary>
        /// <param name="componentTypeID">A GUID specifying a specific <see cref="componentTypeID"/> class</param>
        /// <returns>An instance of <see cref="ComponentType"/></returns>
        public ComponentType GetSingle(Guid componentTypeID)
        {
            this._logService.Debug("BrandService.GetSingle called.");

            if (componentTypeID == Guid.Empty)
            {
                throw new ArgumentException(nameof(componentTypeID));
            }

            var componentType = this._componentTypeRepository.GetSingle(componentTypeID);

            if (componentType == null)
            {
                this._logService.Verbose("ComponentTypeService.GetSingle returned 0 records.");
            }
            else
            {
                this._logService.Verbose("ComponentTypeService.GetSingle returned 1 record.");
            }

            return componentType;
        }
    }
}