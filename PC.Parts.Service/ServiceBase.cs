﻿// <copyright file="ServiceBase.cs" company="EveryWare">
//     Copyright(c) EveryWare. All rights reserved.
// </copyright >

namespace PC.Parts.Service
{
    using Common.Interfaces.Logging;

    /// <summary>
    /// Service base class (abstract)
    /// </summary>
    public abstract class ServiceBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceBase"/> class
        /// </summary>
        /// <param name="logService">An instance of the <see cref="ILogService"/> class</param>
        public ServiceBase(ILogService logService)
        {
            this._logService = logService;
        }

        /// <summary>
        /// Gets an implementation of the <see cref="ILogService"/> interface
        /// </summary>
        protected ILogService _logService { get; }
    }
}
