﻿// <copyright file="BrandService.cs" company="EveryWare">
//     Copyright(c) EveryWare. All rights reserved.
// </copyright >

namespace PC.Parts.Service
{
    using System;
    using System.Collections.Generic;
    using Common.Interfaces.Logging;
    using Common.Interfaces.Repository;
    using Common.Interfaces.Service;
    using Common.Models;

    /// <summary>
    /// Brand Service class
    /// </summary>
    public class BrandService : ServiceBase, IBrandService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BrandService"/> class
        /// </summary>
        /// <param name="brandRepository">An implementation of the <see cref="IBrandRepository"/> interface</param>
        /// <param name="logService">An implementation of the <see cref="ILogService"/> interface</param>
        public BrandService(IBrandRepository brandRepository, ILogService logService) : base(logService)
        {
            this._brandRepository = brandRepository;
        }

        /// <summary>
        /// Gets an implementation of the <see cref="IBrandRepository"/> interface
        /// </summary>
        private IBrandRepository _brandRepository { get; }

        /// <summary>
        /// Returns a list of all existing Brands
        /// </summary>
        /// <returns>A List of <see cref="Brand"/></returns>
        public List<Brand> Get()
        {
            this._logService.Debug("BrandService.Get called.");

            var brands = new List<Brand>();

            brands = this._brandRepository.Get();

            this._logService.Verbose("BrandService.Get returned {0} records.", brands.Count);

            return brands;
        }

        /// <summary>
        /// Returns a Brand record
        /// </summary>
        /// <param name="brandID">A GUID specifying a specific <see cref="brandID"/> class</param>
        /// <returns>An instance of <see cref="Brand"/></returns>
        public Brand GetSingle(Guid brandID)
        {
            this._logService.Debug("BrandService.GetSingle called.");

            if (brandID == Guid.Empty)
            {
                throw new ArgumentException(nameof(brandID));
            }

            var brand = this._brandRepository.GetSingle(brandID);

            if (brand == null)
            {
                this._logService.Verbose("BrandService.GetSingle returned 0 records.");
            }
            else
            {
                this._logService.Verbose("BrandService.GetSingle returned 1 record.");
            }

            return brand;
        }
    }
}