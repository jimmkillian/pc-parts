﻿// <copyright file="ComponentService.cs" company="EveryWare">
//     Copyright(c) EveryWare. All rights reserved.
// </copyright >

namespace PC.Parts.Service
{
    using System;
    using System.Collections.Generic;
    using Common;
    using Common.Interfaces.Logging;
    using Common.Interfaces.Repository;
    using Common.Interfaces.Service;
    using Common.Models;
    using FluentValidation;

    /// <summary>
    /// Component Service class
    /// </summary>
    public class ComponentService : ServiceBase, IComponentService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentService"/> class
        /// </summary>
        /// <param name="componentRepository">An implementation of the <see cref="IComponentRepository"/> interface</param>
        /// <param name="logService">An implementation of the <see cref="ILogService"/> interface</param>
        public ComponentService(IComponentRepository componentRepository, ILogService logService) : base(logService)
        {
            this._componentRepository = componentRepository;
            this._componentValidator = new ComponentValidator();
        }

        /// <summary>
        /// Gets an implementation of the <see cref="IComponentRepository"/> interface
        /// </summary>
        private IComponentRepository _componentRepository { get; }

        /// <summary>
        /// Gets an instance of the <see cref="ComponentValidator"/> class
        /// </summary>
        private ComponentValidator _componentValidator { get; }

        /// <summary>
        /// Returns a list of all existing Components
        /// </summary>
        /// <returns>A List of <see cref="Component"/></returns>
        public List<Component> Get()
        {
            this._logService.Debug("ComponentService.Get called.");

            var components = new List<Component>();

            components = this._componentRepository.Get();

            this._logService.Verbose("ComponentService.Get returned {0} records.", components.Count);

            return components;
        }

        /// <summary>
        /// Returns a Component record
        /// </summary>
        /// <param name="componentID">A GUID specifying a specific <see cref="componentID"/> class</param>
        /// <returns>An instance of <see cref="Component"/></returns>
        public Component GetSingle(Guid componentID)
        {
            this._logService.Debug("ComponentService.GetSingle called.");

            if (componentID == Guid.Empty)
            {
                throw new ArgumentException(nameof(componentID));
            }

            var component = this._componentRepository.GetSingle(componentID);

            if (component == null)
            {
                this._logService.Verbose("ComponentService.GetSingle returned 0 records.");
            }
            else
            {
                this._logService.Verbose("ComponentService.GetSingle returned 1 record.");
            }

            return component;
        }

        /// <summary>
        /// Returns a ServiceResult
        /// </summary>
        /// <param name="component">A Component to insert</param>
        /// <returns>A <see cref="ServiceResult"/></returns>
        public ServiceResult<Component> Insert(Component component)
        {
            if (component == null)
            {
                throw new ArgumentNullException(nameof(component));
            }
            
            this._logService.Debug("ComponentService.Insert called.");

            var serviceResult = this._componentValidator.ValidateComponent(component);

            if (!serviceResult.IsValid)
            {
                this._logService.Debug("ComponentService.Insert failed validation.");
                return serviceResult;
            }
            
            this._logService.Debug("ComponentService.Insert passed validation.");

            serviceResult.IsSuccessful = this._componentRepository.Insert(component);
            
            if (serviceResult.IsSuccessful)
            {
                this._logService.Debug("ComponentService.Insert succeeded.");
            }
            else
            {
                this._logService.Debug("ComponentService.Insert failed.");
            }

            return serviceResult;
        }

        /// <summary>
        /// Returns a ServiceResult
        /// </summary>
        /// <param name="component">A Component to update</param>
        /// <returns>A <see cref="ServiceResult"/></returns>
        public ServiceResult<Component> Update(Component component)
        {
            if (component == null)
            {
                throw new ArgumentNullException(nameof(component));
            }

            this._logService.Debug("ComponentService.Update called.");

            var serviceResult = this._componentValidator.ValidateComponent(component);

            if (!serviceResult.IsValid)
            {
                this._logService.Verbose("ComponentService.Update failed validation.");
                return serviceResult;
            }

            this._logService.Verbose("ComponentService.Update passed validation.");

            serviceResult.IsSuccessful = this._componentRepository.Update(component);

            if (serviceResult.IsSuccessful)
            {
                this._logService.Verbose("ComponentService.Update succeeded.");
            }
            else
            {
                this._logService.Verbose("ComponentService.Update failed.");
            }

            return serviceResult;
        }

        /// <summary>
        /// Returns a ServiceResult
        /// </summary>
        /// <param name="componentID">A GUID specifying which Component to delete</param>
        /// <returns>A <see cref="ServiceResult"/></returns>
        public ServiceResult<Component> Delete(Guid componentID)
        {
            this._logService.Debug("ComponentService.Delete called.");

            if (componentID == Guid.Empty)
            {
                throw new ArgumentException(nameof(componentID));
            }

            var serviceResult = new ServiceResult<Component>();

            serviceResult.IsSuccessful = this._componentRepository.Delete(componentID);

            if (serviceResult.IsSuccessful)
            {
                this._logService.Verbose("ComponentService.Delete succeeded.");
            }
            else
            {
                this._logService.Verbose("ComponentService.Delete failed.");
            }

            return serviceResult;
        }

        /// <summary>
        /// Component validator (Internal)
        /// </summary>
        internal class ComponentValidator : AbstractValidator<Component>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ComponentValidator"/> class
            /// </summary>
            public ComponentValidator()
            {
                RuleFor(f => f.ComponentID).NotNull().NotEmpty();
                RuleFor(f => f.ComponentName).NotNull().NotEmpty().MaximumLength(25);
                RuleFor(f => f.ComponentDescription).NotNull().NotEmpty().MaximumLength(50);
                RuleFor(f => f.ComponentPrice).NotNull().NotEmpty().NotEqual(0);
                RuleFor(f => f.BrandID).NotNull().NotEmpty();
                RuleFor(f => f.ComponentTypeID).NotNull().NotEmpty();
            }

            /// <summary>
            /// Validates a Component
            /// </summary>
            /// <param name="component">An instance of the <see cref="component"/> class</param>
            /// <returns>An instance of the <see cref="ServiceResult{Component}"/> class</returns>
            public ServiceResult<Component> ValidateComponent(Component component)
            {
                var serviceResult = new ServiceResult<Component>()
                {
                    Object = component
                };

                var result = this.Validate(component);

                serviceResult.IsValid = result.IsValid;

                if (!result.IsValid)
                {
                    foreach (var error in result.Errors)
                    {
                        serviceResult.Messages.Add(new Message() { MessageText = error.ErrorMessage, FieldName = error.PropertyName });
                    }
                }

                return serviceResult;
            }
        }
    }
}