﻿// <copyright file="BrandRepository.cs" company="EveryWare">
//     Copyright(c) EveryWare. All rights reserved.
// </copyright >

namespace PC.Parts.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using Common;
    using Common.Interfaces.Logging;
    using Common.Interfaces.Repository;
    using Common.Models;

    /// <summary>
    /// Brand Repository class
    /// </summary>
    public class BrandRepository : RepositoryBase, IBrandRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BrandRepository"/> class
        /// </summary>
        /// <param name="logService">An implementation of the <see cref="ILogService"/> interface</param>
        public BrandRepository(ILogService logService) : base(logService)
        {
        }

        /// <inheritdoc />
        public List<Brand> Get()
        {
            this._logService.Debug("BrandRepository.Get called.");

            var sql = "SELECT BrandID, BrandName FROM Brand ORDER BY BrandName";

            List<Brand> brands = new List<Brand>();
            
            this._logService.Debug("BrandRepository.Get, SQL: {0}", sql);

            using (SqlConnection connection = new SqlConnection(Settings.ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.CommandType = CommandType.Text;

                    connection.Open();

                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            brands.Add(this.GetBrand(dataReader));
                        }
                    }
                }
            }
            
            this._logService.Verbose("BrandRepository.Get returned {0} records.", brands.Count);

            return brands;
        }

        /// <inheritdoc />
        public Brand GetSingle(Guid brandID)
        {
            this._logService.Debug("BrandRepository.GetSingle called.");

            if (brandID == Guid.Empty)
            {
                throw new ArgumentException(nameof(brandID));
            }
            
            var sql = "SELECT BrandID, BrandName FROM Brand WHERE BrandID = @BrandID";

            Brand brand = null;
            
            this._logService.Verbose("BrandRepository.GetSingle, SQL: {0}", sql);

            using (SqlConnection connection = new SqlConnection(Settings.ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.CommandType = CommandType.Text;

                    command.Parameters.AddWithValue("@BrandID", brandID);

                    connection.Open();

                    using (var dataReader = command.ExecuteReader())
                    {
                        if (dataReader.Read())
                        {
                            brand = this.GetBrand(dataReader);
                        }
                    }
                }
            }

            if (brand == null)
            {
                this._logService.Verbose("BrandRepository.GetSingle returned 0 records.");
            }
            else
            {
                this._logService.Verbose("BrandRepository.GetSingle returned 1 record.");
            }

            return brand;
        }

        /// <summary>
        /// Converts a data reader record into a Brand
        /// </summary>
        /// <param name="dataReader">An instance of the <see cref="SqlDataReader"/> class</param>
        /// <returns>A list of <see cref="Brand"/> instances</returns>
        private Brand GetBrand(SqlDataReader dataReader)
        {
            return new Brand()
            {
                BrandID = (Guid)dataReader["BrandID"],
                BrandName = (string)dataReader["BrandName"]
            };
        }

        /// <summary>
        /// Converts a Brand into SQL Parameters 
        /// </summary>
        /// <param name="command">An instance of the <see cref="SqlCommand"/> class</param>
        /// <param name="brand">An instance of the <see cref="Brand"/> class</param>
        /// <returns>An instance of the <see cref="SqlCommand"/> class with parameters added</returns>
        private SqlCommand GetParameters(SqlCommand command, Brand brand)
        {
            command.Parameters.AddWithValue("@BrandID", brand.BrandID);
            command.Parameters.AddWithValue("@BrandName", brand.BrandName);

            return command;
        }
    }
}
