﻿// <copyright file="RepositoryBase.cs" company="EveryWare">
//     Copyright(c) EveryWare. All rights reserved.
// </copyright >

namespace PC.Parts.Repository
{
    using Common.Interfaces.Logging;

    /// <summary>
    /// Repository base class (abstract)
    /// </summary>
    public abstract class RepositoryBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryBase"/> class
        /// </summary>
        /// <param name="logService">An instance of the <see cref="ILogService"/> class</param>
        public RepositoryBase(ILogService logService)
        {
            this._logService = logService;
        }

        /// <summary>
        /// Gets an implementation of the <see cref="ILogService"/> interface
        /// </summary>
        protected ILogService _logService { get; }
    }
}
