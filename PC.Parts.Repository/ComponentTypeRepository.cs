﻿// <copyright file="ComponentTypeRepository.cs" company="EveryWare">
//     Copyright(c) EveryWare. All rights reserved.
// </copyright >

namespace PC.Parts.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using Common;
    using Common.Interfaces.Logging;
    using Common.Interfaces.Repository;
    using Common.Models;

    /// <summary>
    /// ComponentType Repository class
    /// </summary>
    public class ComponentTypeRepository : RepositoryBase, IComponentTypeRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentTypeRepository"/> class
        /// </summary>
        /// <param name="logService">An implementation of the <see cref="ILogService"/> interface</param>
        public ComponentTypeRepository(ILogService logService) : base(logService)
        {
        }

        /// <inheritdoc />
        public List<ComponentType> Get()
        {
            this._logService.Debug("ComponentTypeRepository.Get called.");

            var sql = "SELECT ComponentTypeID, ComponentTypeName FROM ComponentType ORDER BY ComponentTypeName";

            List<ComponentType> componentTypes = new List<ComponentType>();
            
            this._logService.Debug("ComponentTypeRepository.Get, SQL: {0}", sql);

            using (SqlConnection connection = new SqlConnection(Settings.ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.CommandType = CommandType.Text;

                    connection.Open();

                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            componentTypes.Add(this.GetComponentType(dataReader));
                        }
                    }
                }
            }
            
            this._logService.Verbose("ComponentTypeRepository.Get returned {0} records.", componentTypes.Count);

            return componentTypes;
        }

        /// <inheritdoc />
        public ComponentType GetSingle(Guid componentTypeID)
        {
            this._logService.Debug("ComponentTypeRepository.GetSingle called.");

            if (componentTypeID == Guid.Empty)
            {
                throw new ArgumentException(nameof(componentTypeID));
            }
            
            var sql = "SELECT ComponentTypeID, ComponentTypeName FROM ComponentType WHERE ComponentTypeID = @ComponentTypeID";

            ComponentType componentType = null;
            
            this._logService.Verbose("ComponentTypeRepository.GetSingle, SQL: {0}", sql);

            using (SqlConnection connection = new SqlConnection(Settings.ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.CommandType = CommandType.Text;

                    command.Parameters.AddWithValue("@ComponentTypeID", componentTypeID);

                    connection.Open();

                    using (var dataReader = command.ExecuteReader())
                    {
                        if (dataReader.Read())
                        {
                            componentType = this.GetComponentType(dataReader);
                        }
                    }
                }
            }

            if (componentType == null)
            {
                this._logService.Verbose("ComponentTypeRepository.GetSingle returned 0 records.");
            }
            else
            {
                this._logService.Verbose("ComponentTypeRepository.GetSingle returned 1 record.");
            }

            return componentType;
        }

        /// <summary>
        /// Converts a data reader record into a ComponentType
        /// </summary>
        /// <param name="dataReader">An instance of the <see cref="SqlDataReader"/> class</param>
        /// <returns>A list of <see cref="ComponentType"/> instances</returns>
        private ComponentType GetComponentType(SqlDataReader dataReader)
        {
            return new ComponentType()
            {
                ComponentTypeID = (Guid)dataReader["ComponentTypeID"],
                ComponentTypeName = (string)dataReader["ComponentTypeName"]
            };
        }

        /// <summary>
        /// Converts a ComponentType into SQL Parameters 
        /// </summary>
        /// <param name="command">An instance of the <see cref="SqlCommand"/> class</param>
        /// <param name="componentType">An instance of the <see cref="ComponentType"/> class</param>
        /// <returns>An instance of the <see cref="SqlCommand"/> class with parameters added</returns>
        private SqlCommand GetParameters(SqlCommand command, ComponentType componentType)
        {
            command.Parameters.AddWithValue("@ComponentTypeID", componentType.ComponentTypeID);
            command.Parameters.AddWithValue("@ComponentTypeName", componentType.ComponentTypeName);

            return command;
        }
    }
}
