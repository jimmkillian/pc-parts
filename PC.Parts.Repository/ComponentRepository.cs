﻿// <copyright file="ComponentRepository.cs" company="EveryWare">
//     Copyright(c) EveryWare. All rights reserved.
// </copyright >

namespace PC.Parts.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using Common;
    using Common.Interfaces.Logging;
    using Common.Interfaces.Repository;
    using Common.Models;

    /// <summary>
    /// Component Repository class
    /// </summary>
    public class ComponentRepository : RepositoryBase, IComponentRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentRepository"/> class
        /// </summary>
        /// <param name="logService">An implementation of the <see cref="ILogService"/> interface</param>
        public ComponentRepository(ILogService logService) : base(logService)
        {
        }

        /// <inheritdoc />
        public List<Component> Get()
        {
            this._logService.Debug("ComponentRepository.Get called.");

            var sql = "SELECT ComponentID, ComponentName, ComponentDescription, ComponentPrice, BrandID, ComponentTypeID FROM Component ORDER BY ComponentName";

            List<Component> components = new List<Component>();
            
            this._logService.Debug("ComponentRepository.Get, SQL: {0}", sql);

            using (SqlConnection connection = new SqlConnection(Settings.ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.CommandType = CommandType.Text;

                    connection.Open();

                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            components.Add(this.GetComponent(dataReader));
                        }
                    }
                }
            }
            
            this._logService.Verbose("ComponentRepository.Get returned {0} records.", components.Count);

            return components;
        }

        /// <inheritdoc />
        public Component GetSingle(Guid componentID)
        {
            this._logService.Debug("ComponentRepository.GetSingle called.");

            if (componentID == Guid.Empty)
            {
                throw new ArgumentException(nameof(componentID));
            }
            
            var sql = "SELECT ComponentID, ComponentName, ComponentDescription, ComponentPrice, BrandID, ComponentTypeID FROM Component WHERE ComponentID = @ComponentID";

            Component component = null;
            
            this._logService.Verbose("ComponentRepository.GetSingle, SQL: {0}", sql);

            using (SqlConnection connection = new SqlConnection(Settings.ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.CommandType = CommandType.Text;

                    command.Parameters.AddWithValue("@ComponentID", componentID);

                    connection.Open();

                    using (var dataReader = command.ExecuteReader())
                    {
                        if (dataReader.Read())
                        {
                            component = this.GetComponent(dataReader);
                        }
                    }
                }
            }

            if (component == null)
            {
                this._logService.Verbose("ComponentRepository.GetSingle returned 0 records.");
            }
            else
            {
                this._logService.Verbose("ComponentRepository.GetSingle returned 1 record.");
            }

            return component;
        }

        /// <inheritdoc />
        public bool Insert(Component component)
        {
            this._logService.Debug("ComponentRepository.Insert called.");

            if (component == null)
            {
                throw new ArgumentNullException(nameof(component));
            }

            var sql = "INSERT INTO Component(ComponentID, ComponentName, ComponentDescription, ComponentPrice, BrandID, ComponentTypeID) " +
                      "VALUES (@ComponentID, @ComponentName, @ComponentDescription, @ComponentPrice, @BrandID, @ComponentTypeID)";

            this._logService.Verbose("ComponentRepository.Insert, SQL: {0}", sql);

            using (SqlConnection connection = new SqlConnection(Settings.ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.CommandType = CommandType.Text;

                    this.GetParameters(command, component);

                    connection.Open();

                    var result = command.ExecuteNonQuery();

                    if (result == 1)
                    {
                        this._logService.Information("ComponentRepository.Insert successful.");
                        return true;
                    }
                    else
                    {
                        this._logService.Verbose("ComponentRepository.Insert Failed.");
                        return false;
                    }
                }
            }
        }

        /// <inheritdoc />
        public bool Update(Component component)
        {
            this._logService.Debug("ComponentRepository.Update called.");

            if (component == null)
            {
                throw new ArgumentNullException(nameof(component));
            }

            var sql = "UPDATE Component SET " +
                      "ComponentName = @ComponentName " +
                      ",ComponentDescription = @ComponentDescription " +
                      ",ComponentPrice = @ComponentPrice " +
                      ",BrandID = @BrandID " +
                      ",ComponentTypeID = @ComponentTypeID " +
                      "WHERE ComponentID = @ComponentID";

            this._logService.Verbose("ComponentRepository.Update, SQL: {0}", sql);

            using (SqlConnection connection = new SqlConnection(Settings.ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.CommandType = CommandType.Text;

                    this.GetParameters(command, component);

                    connection.Open();

                    var result = command.ExecuteNonQuery();

                    if (result == 1)
                    {
                        this._logService.Verbose("ComponentRepository.Update successful.");
                        return true;
                    }
                    else
                    {
                        this._logService.Verbose("ComponentRepository.Update Failed.");
                        return false;
                    }
                }
            }
        }

        /// <inheritdoc />
        public bool Delete(Guid componentID)
        {
            this._logService.Debug("ComponentRepository.Delete called.");

            if (componentID == Guid.Empty)
            {
                throw new ArgumentException(nameof(componentID));
            }

            var sql = "DELETE FROM Component " +
                      "WHERE ComponentID = @ComponentID";

            this._logService.Verbose("ComponentRepository.Delete, SQL: {0}", sql);

            using (SqlConnection connection = new SqlConnection(Settings.ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.CommandType = CommandType.Text;

                    command.Parameters.AddWithValue("@ComponentID", componentID);

                    connection.Open();

                    var result = command.ExecuteNonQuery();

                    if (result == 1)
                    {
                        this._logService.Verbose("ComponentRepository.Delete successful.");
                        return true;
                    }
                    else
                    {
                        this._logService.Verbose("ComponentRepository.Delete failed.");
                        return false;
                    }
                }
            }
        }

        /// <summary>
        /// Converts a data reader record into a Component
        /// </summary>
        /// <param name="dataReader">An instance of the <see cref="SqlDataReader"/> class</param>
        /// <returns>A list of <see cref="Component"/> instances</returns>
        private Component GetComponent(SqlDataReader dataReader)
        {
            return new Component()
            {
                ComponentID = (Guid)dataReader["ComponentID"],
                ComponentName = (string)dataReader["ComponentName"],
                ComponentDescription = (string)dataReader["ComponentDescription"],
                ComponentPrice = (decimal)dataReader["ComponentPrice"],
                BrandID = (Guid)dataReader["BrandID"],
                ComponentTypeID = (Guid)dataReader["ComponentTypeID"]
            };
        }

        /// <summary>
        /// Converts a Component into SQL Parameters 
        /// </summary>
        /// <param name="command">An instance of the <see cref="SqlCommand"/> class</param>
        /// <param name="component">An instance of the <see cref="Component"/> class</param>
        /// <returns>An instance of the <see cref="SqlCommand"/> class with parameters added</returns>
        private SqlCommand GetParameters(SqlCommand command, Component component)
        {
            command.Parameters.AddWithValue("@ComponentID", component.ComponentID);
            command.Parameters.AddWithValue("@ComponentName", component.ComponentName);
            command.Parameters.AddWithValue("@ComponentDescription", component.ComponentDescription);
            command.Parameters.AddWithValue("@ComponentPrice", component.ComponentPrice);
            command.Parameters.AddWithValue("@BrandID", component.BrandID);
            command.Parameters.AddWithValue("@ComponentTypeID", component.ComponentTypeID);

            return command;
        }
    }
}
