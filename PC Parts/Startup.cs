// <copyright file="Startup.cs" company="EveryWare">
//     Copyright(c) EveryWare. All rights reserved.
// </copyright >

namespace PC.Parts.Api
{
    using Common.Interfaces.Logging;
    using Common.Interfaces.Repository;
    using Common.Interfaces.Service;
    using Common.Logging;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.OpenApi.Models;
    using Repository;
    using Service;


    /// <summary>
    /// Startup class
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class
        /// </summary>
        /// <param name="configuration">An implementation of the <see cref="IConfiguration"/> interface</param>
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        /// <summary>
        /// Gets an implementation of the <see cref="IConfiguration"/> interface
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services">An implementation of the <see cref="IServiceCollection"/> interface</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<ILogService, LogService>();

            // Repository Configuration
            services.AddTransient<IBrandRepository, BrandRepository>();
            services.AddTransient<IComponentTypeRepository, ComponentTypeRepository>();
            services.AddTransient<IComponentRepository, ComponentRepository>();

            // Service Configuration
            services.AddTransient<IBrandService, BrandService>();
            services.AddTransient<IComponentTypeService, ComponentTypeService>();
            services.AddTransient<IComponentService, ComponentService>();

            services.AddControllers();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(
                    "v1",
                    new OpenApiInfo
                    {
                        Title = "PC Parts",
                        Version = "v1",
                        Description = "PC Parts API"
                    });
            });
            
            services.AddCors(c =>
            {
                c.AddPolicy("AllowOrigin", options => options.AllowAnyOrigin());
            });
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app">An implementation of the <see cref="IApplicationBuilder"/> interface</param>
        /// <param name="env">An implementation of the <see cref="IWebHostEnvironment"/> interface</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "PCParts API v1");
                c.RoutePrefix = string.Empty;
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            app.UseCors(options => options.AllowAnyOrigin());
            
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
