﻿// <copyright file="LoggingController.cs" company="EveryWare">
//     Copyright(c) EveryWare. All rights reserved.
// </copyright >

namespace PC.Parts.Api.Controllers
{
    using Common.Interfaces.Logging;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Logging controller
    /// </summary>
    public abstract class LoggingController : ControllerBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoggingController"/> class
        /// </summary>
        /// <param name="logService">An instance of the <see cref="ILogService"/> class</param>
        public LoggingController(ILogService logService)
        {
            this._logService = logService;
        }

        /// <summary>
        /// Gets an implementation of the <see cref="ILogService"/> interface
        /// </summary>
        protected ILogService _logService { get; }
    }
}
