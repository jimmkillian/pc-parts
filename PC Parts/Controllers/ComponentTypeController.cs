﻿// <copyright file="ComponentTypeController.cs" company="EveryWare">
//     Copyright(c) EveryWare. All rights reserved.
// </copyright >

namespace PC.Parts.Api.Controllers
{
    using System;
    using Common.Interfaces.Logging;
    using Common.Interfaces.Service;
    using Common.Logging;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// ComponentType Controller
    /// </summary>
    [ApiController]
    [Route("ComponentTypes")]
    public class ComponentTypeController : LoggingController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentTypeController"/> class
        /// </summary>
        /// <param name="componentTypeService">An implementation of the <see cref="IComponentTypeService"/> interface</param>
        /// <param name="logService">An implementation of the <see cref="ILogService"/> interface</param>
        public ComponentTypeController(IComponentTypeService componentTypeService, ILogService logService) : base(logService)
        {
            this._componentTypeService = componentTypeService;
        }

        /// <summary>
        /// Gets an implementation of the <see cref="IComponentTypeService"/> interface
        /// </summary>
        private IComponentTypeService _componentTypeService { get; }

        /// <summary>
        /// Gets a list of all ComponentTypes
        /// </summary>
        /// <returns>An <see cref="ActionResult"/> of a List of instances of the <see cref="ComponentType"/> class</returns>
        [HttpGet]
        public ActionResult Get()
        {
            try
            {
                this._logService.Debug("ComponentTypeController.GetAll called.");

                var componentTypes = this._componentTypeService.Get();

                this._logService.Verbose("ComponentTypeController.GetAll returned ComponentType record/s.", componentTypes.Count);

                return this.StatusCode(200, componentTypes);
            }
            catch (ArgumentNullException argumentNullException)
            {
                this._logService.Error(LogBuilder.GetLog(argumentNullException));
                return this.StatusCode(400);
            }
            catch (ArgumentException argumentException)
            {
                this._logService.Error(LogBuilder.GetLog(argumentException));
                return this.StatusCode(400);
            }
            catch (Exception exception)
            {
                this._logService.Error(LogBuilder.GetLog(exception));
                return this.StatusCode(500);
            }
        }

        /// <summary>
        /// Gets a ComponentType
        /// </summary>
        /// <param name="componentTypeID">The ComponentType ID</param>
        /// <returns>>An <see cref="ActionResult"/> of an instance of the <see cref="ComponentType"/> class</returns>
        [HttpGet("{componentTypeID}")]
        public ActionResult GetSingle(Guid componentTypeID)
        {
            try
            {
                if (componentTypeID == Guid.Empty)
                {
                    throw new ArgumentException(nameof(componentTypeID));
                }

                this._logService.Debug("ComponentTypeController.Get called.");

                var componentType = this._componentTypeService.GetSingle(componentTypeID);

                if (componentType == null)
                {
                    this._logService.Verbose("ComponentTypeController.Get returned 0 records.");
                }
                else
                {
                    this._logService.Verbose("ComponentTypeController.Get returned 1 record.");
                }

                return this.StatusCode(200, componentType);
            }
            catch (ArgumentNullException argumentNullException)
            {
                this._logService.Error(LogBuilder.GetLog(argumentNullException));
                return this.StatusCode(400);
            }
            catch (ArgumentException argumentException)
            {
                this._logService.Error(LogBuilder.GetLog(argumentException));
                return this.StatusCode(400);
            }
            catch (Exception exception)
            {
                this._logService.Error(LogBuilder.GetLog(exception));
                return this.StatusCode(500);
            }
        }
    }
}