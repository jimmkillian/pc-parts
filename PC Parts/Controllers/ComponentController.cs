﻿// <copyright file="ComponentController.cs" company="EveryWare">
//     Copyright(c) EveryWare. All rights reserved.
// </copyright >

namespace PC.Parts.Api.Controllers
{
    using System;
    using Common.Interfaces.Logging;
    using Common.Interfaces.Service;
    using Common.Logging;
    using Common.Models;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Component Controller
    /// </summary>
    [ApiController]
    [Route("Components")]
    public class ComponentController : LoggingController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentController"/> class
        /// </summary>
        /// <param name="componentService">An implementation of the <see cref="IComponentService"/> interface</param>
        /// <param name="logService">An implementation of the <see cref="ILogService"/> interface</param>
        public ComponentController(IComponentService componentService, ILogService logService) : base(logService)
        {
            this._componentService = componentService;
        }

        /// <summary>
        /// Gets an implementation of the <see cref="IComponentService"/> interface
        /// </summary>
        private IComponentService _componentService { get; }

        /// <summary>
        /// Gets a list of all Components
        /// </summary>
        /// <returns>An <see cref="ActionResult"/> of a List of instances of the <see cref="Component"/> class</returns>
        [HttpGet]
        public ActionResult Get()
        {
            try
            {
                this._logService.Debug("ComponentController.GetAll called.");

                var components = this._componentService.Get();

                this._logService.Verbose("ComponentController.GetAll returned Component record/s.", components.Count);

                return this.StatusCode(200, components);
            }
            catch (ArgumentNullException argumentNullException)
            {
                this._logService.Error(LogBuilder.GetLog(argumentNullException));
                return this.StatusCode(400);
            }
            catch (ArgumentException argumentException)
            {
                this._logService.Error(LogBuilder.GetLog(argumentException));
                return this.StatusCode(400);
            }
            catch (Exception exception)
            {
                this._logService.Error(LogBuilder.GetLog(exception));
                return this.StatusCode(500);
            }
        }

        /// <summary>
        /// Gets a Component
        /// </summary>
        /// <param name="componentID">The Component ID</param>
        /// <returns>>An <see cref="ActionResult"/> of an instance of the <see cref="Component"/> class</returns>
        [HttpGet("{componentID}")]
        public ActionResult GetSingle(Guid componentID)
        {
            try
            {
                if (componentID == Guid.Empty)
                {
                    throw new ArgumentException(nameof(componentID));
                }

                this._logService.Debug("ComponentController.Get called.");

                var component = this._componentService.GetSingle(componentID);

                if (component == null)
                {
                    this._logService.Verbose("ComponentController.Get returned 0 records.");
                }
                else
                {
                    this._logService.Verbose("ComponentController.Get returned 1 record.");
                }

                return this.StatusCode(200, component);
            }
            catch (ArgumentNullException argumentNullException)
            {
                this._logService.Error(LogBuilder.GetLog(argumentNullException));
                return this.StatusCode(400);
            }
            catch (ArgumentException argumentException)
            {
                this._logService.Error(LogBuilder.GetLog(argumentException));
                return this.StatusCode(400);
            }
            catch (Exception exception)
            {
                this._logService.Error(LogBuilder.GetLog(exception));
                return this.StatusCode(500);
            }
        }

        /// <summary>
        /// Inserts a new Component record
        /// </summary>
        /// <param name="component">An instance of the <see cref="Component"/> class</param>
        /// <returns>An instance of an <see cref="ActionResult"/> of an instance of the <see cref="ServiceResult{Component}"/> class</returns>
        [HttpPost]
        public ActionResult Post(Component component)
        {
            try
            {
                if (component == null)
                {
                    throw new ArgumentNullException(nameof(component));
                }

                this._logService.Debug("ComponentController.Post called.");

                var serviceResult = this._componentService.Insert(component);

                if (serviceResult.IsSuccessful)
                {
                    this._logService.Verbose("ComponentController.Post succeeded.");
                    return this.StatusCode(201, serviceResult);
                }
                else
                {
                    this._logService.Verbose("ComponentController.Post failed.");
                    return this.StatusCode(400, serviceResult);
                }
            }
            catch (ArgumentNullException argumentNullException)
            {
                this._logService.Error(LogBuilder.GetLog(argumentNullException));
                return this.StatusCode(400);
            }
            catch (ArgumentException argumentException)
            {
                this._logService.Error(LogBuilder.GetLog(argumentException));
                return this.StatusCode(400);
            }
            catch (Exception exception)
            {
                this._logService.Error(LogBuilder.GetLog(exception));
                return this.StatusCode(500);
            }
        }

        /// <summary>
        /// Updates a Component record
        /// </summary>
        /// <param name="component">An instance of the <see cref="Component"/> class</param>
        /// <returns>An instance of an <see cref="ActionResult"/> of an instance of the <see cref="ServiceResult{Component}"/> class</returns>
        [HttpPut("{componentID}")]
        public ActionResult Put(Component component)
        {
            try
            {
                if (component == null)
                {
                    throw new ArgumentNullException(nameof(component));
                }

                this._logService.Debug("ComponentController.Put called.");

                var serviceResult = this._componentService.Update(component);

                if (serviceResult.IsSuccessful)
                {
                    this._logService.Verbose("ComponentController.Put succeeded.");
                    return this.StatusCode(200, serviceResult);
                }
                else
                {
                    this._logService.Verbose("ComponentController.Put failed.");
                    return this.StatusCode(400, serviceResult);
                }
            }
            catch (ArgumentNullException argumentNullException)
            {
                this._logService.Error(LogBuilder.GetLog(argumentNullException));
                return this.StatusCode(400);
            }
            catch (ArgumentException argumentException)
            {
                this._logService.Error(LogBuilder.GetLog(argumentException));
                return this.StatusCode(400);
            }
            catch (Exception exception)
            {
                this._logService.Error(LogBuilder.GetLog(exception));
                return this.StatusCode(500);
            }
        }

        /// <summary>
        /// Deletes a Component
        /// </summary>
        /// <param name="componentID">The Component ID</param>
        /// <returns>>An <see cref="ActionResult"/> of an instance of the <see cref="Component"/> class</returns>
        [HttpDelete("{componentID}")]
        public ActionResult Delete(Guid componentID)
        {
            try
            {
                if (componentID == Guid.Empty)
                {
                    throw new ArgumentException(nameof(componentID));
                }

                this._logService.Debug("ComponentController.Delete called.");

                var actor = this._componentService.Delete(componentID);

                if (actor == null)
                {
                    this._logService.Verbose("ComponentController.Delete returned 0 records.");
                }
                else
                {
                    this._logService.Verbose("ComponentController.Delete returned 1 record.");
                }

                return this.StatusCode(200, actor);
            }
            catch (ArgumentNullException argumentNullException)
            {
                this._logService.Error(LogBuilder.GetLog(argumentNullException));
                return this.StatusCode(400);
            }
            catch (ArgumentException argumentException)
            {
                this._logService.Error(LogBuilder.GetLog(argumentException));
                return this.StatusCode(400);
            }
            catch (Exception exception)
            {
                this._logService.Error(LogBuilder.GetLog(exception));
                return this.StatusCode(500);
            }
        }
    }
}
