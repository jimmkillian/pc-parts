﻿// <copyright file="BrandController.cs" company="EveryWare">
//     Copyright(c) EveryWare. All rights reserved.
// </copyright >

namespace PC.Parts.Api.Controllers
{
    using System;
    using Common.Interfaces.Logging;
    using Common.Interfaces.Service;
    using Common.Logging;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Brand Controller
    /// </summary>
    [ApiController]
    [Route("Brands")]
    public class BrandController : LoggingController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BrandController"/> class
        /// </summary>
        /// <param name="brandService">An implementation of the <see cref="IBrandService"/> interface</param>
        /// <param name="logService">An implementation of the <see cref="ILogService"/> interface</param>
        public BrandController(IBrandService brandService, ILogService logService) : base(logService)
        {
            this._brandService = brandService;
        }

        /// <summary>
        /// Gets an implementation of the <see cref="IBrandService"/> interface
        /// </summary>
        private IBrandService _brandService { get; }

        /// <summary>
        /// Gets a list of all Brands
        /// </summary>
        /// <returns>An <see cref="ActionResult"/> of a List of instances of the <see cref="Brand"/> class</returns>
        [HttpGet]
        public ActionResult Get()
        {
            try
            {
                this._logService.Debug("BrandController.GetAll called.");

                var brands = this._brandService.Get();

                this._logService.Verbose("BrandController.GetAll returned Actor record/s.", brands.Count);

                return this.StatusCode(200, brands);
            }
            catch (ArgumentNullException argumentNullException)
            {
                this._logService.Error(LogBuilder.GetLog(argumentNullException));
                return this.StatusCode(400);
            }
            catch (ArgumentException argumentException)
            {
                this._logService.Error(LogBuilder.GetLog(argumentException));
                return this.StatusCode(400);
            }
            catch (Exception exception)
            {
                this._logService.Error(LogBuilder.GetLog(exception));
                return this.StatusCode(500);
            }
        }

        /// <summary>
        /// Gets a Brand
        /// </summary>
        /// <param name="brandID">The Brand ID</param>
        /// <returns>>An <see cref="ActionResult"/> of an instance of the <see cref="Brand"/> class</returns>
        [HttpGet("{brandID}")]
        public ActionResult GetSingle(Guid brandID)
        {
            try
            {
                if (brandID == Guid.Empty)
                {
                    throw new ArgumentException(nameof(brandID));
                }

                this._logService.Debug("BrandController.Get called.");

                var brand = this._brandService.GetSingle(brandID);

                if (brand == null)
                {
                    this._logService.Verbose("BrandController.Get returned 0 records.");
                }
                else
                {
                    this._logService.Verbose("BrandController.Get returned 1 record.");
                }

                return this.StatusCode(200, brand);
            }
            catch (ArgumentNullException argumentNullException)
            {
                this._logService.Error(LogBuilder.GetLog(argumentNullException));
                return this.StatusCode(400);
            }
            catch (ArgumentException argumentException)
            {
                this._logService.Error(LogBuilder.GetLog(argumentException));
                return this.StatusCode(400);
            }
            catch (Exception exception)
            {
                this._logService.Error(LogBuilder.GetLog(exception));
                return this.StatusCode(500);
            }
        }
    }
}